import "./App.css";
import Header from "./components/Header";
import RestaurantList from "./components/RestaurantList";

const App = () => {
  return (
    <div className="App">
      <Header />
      <RestaurantList />
    </div>
  );
};

export default App;
