import React from "react";

const Restaurant = ({name, info}) => {
  return(
    <div className = "card">
      Name: {name}
      <br/>
      Info: {info}
    </div>
  );
};

export default Restaurant;