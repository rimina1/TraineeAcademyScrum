import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import Restaurant from "./Restaurant";
const data = [
  {name : "Katmandu", info : "Nepalilaista ruokaa"},
  {name: "Thai & Laos", info: "Thaimaalaista ruokaa"}
];

const RestaurantList = () => {
  const [restaurantData, setRestaurantData] = useState(data);

  useEffect(() =>{
    setRestaurantData(data);
  }, []);

  return (
    <div className= "restaurant-List">
      {restaurantData.map((restaurant, index) =>
        <Restaurant key = {index} name = {restaurant.name} info = {restaurant.info} />
      )}
    </div>
  );
};

export default RestaurantList;