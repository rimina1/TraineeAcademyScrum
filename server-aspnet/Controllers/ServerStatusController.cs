using Microsoft.AspNetCore.Mvc;

namespace server_aspnet.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ServerStatusController : ControllerBase
    {

        private readonly ILogger<ServerStatusController> _logger;

        public ServerStatusController(ILogger<ServerStatusController> logger)
        {
            _logger = logger;
        }

        [HttpGet()]
        public IActionResult GetAlive()
        {
            _logger.LogInformation("Got alive status request at {}", DateTime.UtcNow);
            return Ok();
        }
    }
}