## To get started
Run `npm install` to install project dependencies.\
Create .env -file to this directory to record environment variables such as\
`APP_PORT=5000`\
`APP_PG_USER=pguser`\
`APP_PG_DB=mydb`\
`APP_PG_PASSWORD=password`\
`APP_PG_PORT=5432`\
Replace the database connection info with the ones that you use and never share those!

## Available Scripts

In the project directory, you can run:

### `npm run dev`

This runs the server in the development mode.\
Open [http://localhost:5000](http://localhost:5000) to view it in your browser.

The server will reload when you make changes.
