import express from "express";
import dotenv from "dotenv";
import cors from "cors";

import mainRouter from "./routes/main.js";
import restaurantRouter from "./routes/restaurants.js";

const app = express();

dotenv.config();
console.log(process.env);
const port = process.env.APP_PORT || 5000;

app.use(
  express.urlencoded({
    extended: false,
  }),
);
app.use(express.json());
app.use(cors());

app.use("/", mainRouter);
app.use("/api/restaurants", restaurantRouter);


app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});